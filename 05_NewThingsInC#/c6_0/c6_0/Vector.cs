﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace c6_0
{
    class Vector
    {
        public int X { get; } = 0;
        public int Y { get; } = 0;
        public Vector(int x, int y)
        {
            X = x;
            Y = y;
        }

        public double Length => Math.Sqrt(X * X + Y * Y);
        public override string ToString() => $"({X}|{Y})";

        public static void ConvertToJSON(List<Vector> vectors)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(XmlConvert.DecodeName());
        }
    }
}
