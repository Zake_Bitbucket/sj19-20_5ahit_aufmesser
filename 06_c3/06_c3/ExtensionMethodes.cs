﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace _06_c3
{
    static class ExtensionMethodes
    {
        public static string Palyndrom(this string data)
        {
            char[] turnAround = data.ToCharArray();
            Array.Reverse(turnAround);
            return new string(turnAround).ToUpper();
        }

        public static int CountThings(this string data, string search)
        {
            return Regex.Matches(data.ToUpper(), search.ToUpper()).Count;
        }
    }
}
