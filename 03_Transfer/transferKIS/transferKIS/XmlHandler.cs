﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;

namespace transferKIS
{
    class XmlHandler
    {
        private static XmlDocument xmlFile { get; set; } = new XmlDocument();
        public XmlHandler(string FilePath)
        {
            //string xml = File.ReadAllText(FilePath);
            xmlFile.Load(FilePath);
        }
        public static double NumOfAllPatients => (double)xmlFile.CreateNavigator().Evaluate("count(//Name)");
        public static double NumOfAllTreatments => (double)xmlFile.CreateNavigator().Evaluate("count(//Behandlung)");
        public static double NumOfDifferentTreatments
        {
            get
            {
                List<string> list;
                list = xmlFile.SelectNodes("//Bezeichnung").Cast<XmlNode>().Select(c => c.InnerText).ToList();
                return list.Distinct().Count();
            }
        }
        public static double NumOfPatientsWithOneTreatment => xmlFile.SelectNodes("//Name[count(Behandlungen/Behandlung) = 1]").Count;
        public static double NumOfPatientsWithTwoTreatments => xmlFile.SelectNodes("//Name[count(Behandlungen/Behandlung) = 2]").Count;
        public static double NumOfPatientsWithMoreTreatments => xmlFile.SelectNodes("//Name[count(Behandlungen/Behandlung) > 2]").Count;
        public static double SumTreatmentTime => Convert.ToDouble(xmlFile.CreateNavigator().Evaluate("sum(//Dauer)"));
        public static List<Models.Patient> GetPatients()
        {
            List<Models.Patient> patients = new List<Models.Patient>();
            XmlNodeList xmlNodeList;
            xmlNodeList = xmlFile.SelectNodes("//Name");
            foreach(XmlNode xmlNode in xmlNodeList)
            {
                int Id = Convert.ToInt32(xmlNode.Attributes[0].Value);
                string LastName = xmlNode.SelectSingleNode("./Nachname").InnerText;
                string FirstName = xmlNode.SelectSingleNode("./Vorname").InnerText;
                string SVNR = xmlNode.SelectSingleNode("./SV").InnerText;
                patients.Add(new Models.Patient(Id, LastName, FirstName, SVNR));
            }
            return patients;
        }
        public static List<Models.Behandlung> GetBehandlungen()
        {
            int count = 1;
            List<Models.Behandlung> behandlungs = new List<Models.Behandlung>();
            List<string> list = xmlFile.SelectNodes("//Bezeichnung").Cast<XmlNode>().Select(c => c.InnerText).ToList();
            foreach (string item in list.Distinct())
            {
                behandlungs.Add(new Models.Behandlung(count, item));
                count++;
            }
            return behandlungs;
        }
        public static List<Models.PatientBehandlung> GetPatientBehandlungs()
        {
            List<Models.PatientBehandlung> patientBehandlungs = new List<Models.PatientBehandlung>();
            XmlNodeList xmlNodeList;
            xmlNodeList = xmlFile.SelectNodes("//Name");
            List<Models.Behandlung> list = XmlHandler.GetBehandlungen();
            foreach (XmlNode xmlNode in xmlNodeList)
            {
                int patienID = Convert.ToInt32(xmlNode.Attributes[0].Value);
                XmlNodeList nodeList = xmlNode.SelectNodes("./Behandlungen/Behandlung");
                foreach (XmlNode xmlNode1 in nodeList)
                {
                    foreach (Models.Behandlung item in list)
                    {
                        if(xmlNode1.SelectSingleNode("./Bezeichnung").InnerText == item.Description)
                        {
                            patientBehandlungs.Add(new Models.PatientBehandlung(patienID, item.ID, Convert.ToInt32(xmlNode1.SelectSingleNode("./Dauer").InnerText), xmlNode1.SelectSingleNode("./Datum").InnerText));
                        }
                    }
                }
                
            }
            return patientBehandlungs;
        }
    }
}
