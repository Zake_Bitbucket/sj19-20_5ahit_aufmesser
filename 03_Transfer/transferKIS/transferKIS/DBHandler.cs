﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace transferKIS
{
    public class DBHandler
    {
        private static string _connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Transfer.accdb; Persist Security Info= False;";
        public static readonly Lazy<OleDbConnection> Connection = new Lazy<OleDbConnection>(CreateConnection); //(using System.Threading)

        public static OleDbConnection CreateConnection()
        {
            var conn = new OleDbConnection(_connectionString);
            conn.Open();
            return conn;
        }
        public static void CloseConnection()
        {
            var conn = Connection.Value;
            conn.Close();
        }

        public static double NumOfAllPatients => executeQuery(@"Select count(*) from Patient");
        public static double NumOfAllTreatments => executeQuery(@"Select count(*) from PatientBehandlungen");
        public static double NumOfDifferentTreatments => executeQuery(@"Select count(*) from Behandlung");
        public static double NumOfPatientsWithOneTreatment => executeQuery(@"SELECT Count(*) FROM (SELECT Count(BehandlungsID), PatientID FROM PatientBehandlungen GROUP BY PatientID HAVING count(BehandlungsID) = 1);");
        public static double NumOfPatientsWithTwoTreatments => executeQuery(@"SELECT Count(*) FROM (SELECT Count(BehandlungsID), PatientID FROM PatientBehandlungen GROUP BY PatientID HAVING count(BehandlungsID) = 2);");
        public static double NumOfPatientsWithMoreTreatments => executeQuery(@"SELECT Count(*) FROM (SELECT Count(BehandlungsID), PatientID FROM PatientBehandlungen GROUP BY PatientID HAVING count(BehandlungsID) > 2);");
        public static double SumTreatmentTime => executeQuery(@"SELECT sum(Dauer) FROM PatientBehandlungen;");

        private static double executeQuery(string query)
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = Connection.Value;
            cmd.CommandText = query;
            double result = Convert.ToDouble(cmd.ExecuteScalar());
            return result;
        }
    }
}
