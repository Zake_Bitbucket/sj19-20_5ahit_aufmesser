﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace transferKIS.Models
{
    class Behandlung
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public Behandlung(int iD, string description)
        {
            ID = iD;
            Description = description;
        }
    }
}
