﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace transferKIS.Models
{
    class PatientBehandlung
    {
        public int PatientID { get; set; }
        public int BehandlungsID { get; set; }
        public int Time { get; set; }
        public string Date { get; set; }

        public PatientBehandlung(int patientID, int behandlungsID, int time, string date)
        {
            PatientID = patientID;
            BehandlungsID = behandlungsID;
            Time = time;
            Date = date;
        }
    }
}
