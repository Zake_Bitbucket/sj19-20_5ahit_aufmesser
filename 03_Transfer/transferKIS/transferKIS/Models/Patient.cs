﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace transferKIS.Models
{
    class Patient
    {
        public int ID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string SVNR { get; set; }
        public Patient(int iD, string lastName, string firstName, string sVNR)
        {
            ID = iD;
            LastName = lastName;
            FirstName = firstName;
            SVNR = sVNR;
        }
    }
}
