﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace transferKIS
{
    /// <summary>
    /// Interaction logic for data.xaml
    /// </summary>
    public partial class data : Window
    {
        public Brush NumOfPatients
        {
            get
            {
                if (lb_numOfPatients.Content.Equals(lb_numOfPatients_DB.Content))
                {
                    return Brushes.Green;
                }
                else
                    return Brushes.Red;
            }
        }
        public Brush NumOfTreatments
        {
            get
            {
                if (lb_numOfTreatments.Content.Equals(lb_numOfTreatments_DB.Content))
                {
                    return Brushes.Green;
                }
                else
                    return Brushes.Red;
            }
        }
        public Brush DifferentTreatments
        {
            get
            {
                if (lb_differentTreatments.Content.Equals(lb_differentTreatments_DB.Content))
                {
                    return Brushes.Green;
                }
                else
                    return Brushes.Red;
            }
        }
        public Brush OneTreatment
        {
            get
            {
                if (lb_oneTreatment.Content.Equals(lb_oneTreatment_DB.Content))
                {
                    return Brushes.Green;
                }
                else
                    return Brushes.Red;
            }
        }
        public Brush TwoTreatments
        {
            get
            {
                if (lb_twoTreatments.Content.Equals(lb_twoTreatments_DB.Content))
                {
                    return Brushes.Green;
                }
                else
                    return Brushes.Red;
            }
        }
        public Brush MoreTreatments
        {
            get
            {
                if (lb_moreTreatments.Content.Equals(lb_moreTreatments_DB.Content))
                {
                    return Brushes.Green;
                }
                else
                    return Brushes.Red;
            }
        }
        public Brush SumOfTreatmentTime
        {
            get
            {
                if (lb_sumOfTreatmentTime.Content.Equals(lb_sumOfTreatmentTime_DB.Content))
                {
                    return Brushes.Green;
                }
                else
                    return Brushes.Red;
            }
        }
        int i = 0;
        public data()
        {
            InitializeComponent();
            bt_Check.IsEnabled = false;
            try
            {
                lb_numOfPatients.Content = XmlHandler.NumOfAllPatients;
                lb_numOfTreatments.Content = XmlHandler.NumOfAllTreatments;
                lb_differentTreatments.Content = XmlHandler.NumOfDifferentTreatments;
                lb_oneTreatment.Content = XmlHandler.NumOfPatientsWithOneTreatment;
                lb_twoTreatments.Content = XmlHandler.NumOfPatientsWithTwoTreatments;
                lb_moreTreatments.Content = XmlHandler.NumOfPatientsWithMoreTreatments;
                lb_sumOfTreatmentTime.Content = XmlHandler.SumTreatmentTime;
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e.Message + "\n" + e.Source, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                //this.close();
            }
        }

        private void bt_transfer_Click(object sender, RoutedEventArgs e)
        {
            bt_transfer.IsEnabled = false;
            bt_Check.IsEnabled = true;
            try
            {
                if (i == 0)
                {
                    TransferHandler.transferAll();
                    lb_numOfPatients_DB.Content = DBHandler.NumOfAllPatients;
                    lb_numOfTreatments_DB.Content = DBHandler.NumOfAllTreatments;
                    lb_differentTreatments_DB.Content = DBHandler.NumOfDifferentTreatments;
                    lb_oneTreatment_DB.Content = DBHandler.NumOfPatientsWithOneTreatment;
                    lb_twoTreatments_DB.Content = DBHandler.NumOfPatientsWithTwoTreatments;
                    lb_moreTreatments_DB.Content = DBHandler.NumOfPatientsWithMoreTreatments;
                    lb_sumOfTreatmentTime_DB.Content = DBHandler.SumTreatmentTime;
                    i++;
                }
                else
                    MessageBox.Show("You can't transfer the xml document twice pls close this window to do so!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message + "\n" + ex.Source, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                //this.close();
            }
        }

        private void bt_Check_Click(object sender, RoutedEventArgs e)
        {
            rec_numPatients.Fill = NumOfPatients;
            rec_numOfTreatments.Fill = NumOfTreatments;
            rec_differentTreatments.Fill = DifferentTreatments;
            rec_oneTreatment.Fill = OneTreatment;
            rec_twoTreatments.Fill = TwoTreatments;
            rec_moreTreatments.Fill = MoreTreatments;
            rec_sumOfTreatmentTime.Fill = SumOfTreatmentTime;
        }
    }
}
