﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace transferKIS
{
    class TransferHandler
    {
        
        public static void transferAll()
        {
            List<Models.Behandlung> treatments = XmlHandler.GetBehandlungen();
            List<Models.Patient> patients = XmlHandler.GetPatients();
            List<Models.PatientBehandlung> patientTreatments = XmlHandler.GetPatientBehandlungs();
            try
            {
                DBHandler.CreateConnection();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = DBHandler.Connection.Value;                

                cmd.CommandText = @"DELETE FROM Patient;";
                cmd.ExecuteNonQuery();
                cmd.CommandText = @"DELETE FROM PatientBehandlungen;";
                cmd.ExecuteNonQuery();
                cmd.CommandText = @"DELETE FROM Behandlung;";
                cmd.ExecuteNonQuery();

                foreach (Models.Patient patient in patients)
                {
                    cmd.CommandText = @"INSERT INTO Patient (Id , Nachname , Vorname, SVNR)
                  VALUES(@Id, @LastName, @FirstName, @SVNR)";

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddRange(new OleDbParameter[]
                    {
                        new OleDbParameter{ ParameterName = "@Id", Value = patient.ID},
                        new OleDbParameter{ ParameterName = "@LastName", Value = patient.LastName  ?? string.Empty},
                        new OleDbParameter{ ParameterName = "@FirstName", Value = patient.FirstName ??  string.Empty},
                        new OleDbParameter{ ParameterName = "@SVNR", Value = patient.SVNR},
                        });

                    cmd.ExecuteNonQuery();
                }
                foreach (Models.Behandlung behandlung in treatments)
                {
                    cmd.CommandText = @"INSERT INTO Behandlung (Id , Bezeichnung)
                  VALUES(@Id, @Bezeichnung)";

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddRange(new OleDbParameter[]
                    {
                        new OleDbParameter{ ParameterName = "@Id", Value = behandlung.ID},
                        new OleDbParameter{ ParameterName = "@Bezeichnung", Value = behandlung.Description  ?? string.Empty},
                        });

                    cmd.ExecuteNonQuery();
                }
                foreach (Models.PatientBehandlung patientBehandlung in patientTreatments)
                {
                    cmd.CommandText = @"INSERT INTO PatientBehandlungen (PatientID, BehandlungsID, Dauer, Datum)
                  VALUES(@PatientId, @BehandlungId, @Dauer, @Datum)";

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddRange(new OleDbParameter[]
                    {
                        new OleDbParameter{ ParameterName = "@PatientId", Value = patientBehandlung.PatientID},
                        new OleDbParameter{ ParameterName = "@BehandlungId", Value = patientBehandlung.BehandlungsID},
                        new OleDbParameter{ ParameterName = "@Dauer", Value = patientBehandlung.Time},
                        new OleDbParameter{ ParameterName = "@Datum", Value = patientBehandlung.Date},
                        });

                    cmd.ExecuteNonQuery();
                }

            }
            catch(Exception e)
            {
                throw e;
            }
        }
    }
}
