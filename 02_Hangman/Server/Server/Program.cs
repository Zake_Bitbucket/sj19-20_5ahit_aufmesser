﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using HangmanServerDLL;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpListener serverSocket = new TcpListener(IPAddress.Parse("127.0.0.1"), 2345);
            TcpClient tcpClient = default;
            int counter = 0;

            ClientHandler clientHandler = new ClientHandler();

            serverSocket.Start();
            Console.WriteLine(">> Server Started!");

            while (true)
            {
                counter++;
                tcpClient = serverSocket.AcceptTcpClient();
                Console.WriteLine(">> Client No: " + counter + " started!");
                clientHandler.startNewClient(tcpClient, Convert.ToString(counter));
            }

        }
    }
}
