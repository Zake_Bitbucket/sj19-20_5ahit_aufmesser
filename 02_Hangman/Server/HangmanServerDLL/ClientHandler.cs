﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HangmanServerDLL
{
    public class ClientHandler
    {
        TcpClient clientSocket;
        string clientNumber;

        public void startNewClient(TcpClient clientSocket, string clientNumber)
        {
            this.clientSocket = clientSocket;
            this.clientNumber = clientNumber;
            Thread clientThread = new Thread(clientHandling);
            clientThread.Start();
        }

        private void clientHandling()
        {
            int requestCount = 0;
            byte[] bytesFromClient = new byte[65536];
            string dataFromClient = null;
            Byte[] sendBytes = null;
            string serverResponse = null;

            while (true)
            {
                try
                {
                    requestCount++;
                    NetworkStream networkStream = clientSocket.GetStream();
                    networkStream.Read(bytesFromClient, 0, (int)clientSocket.ReceiveBufferSize);
                    dataFromClient = Encoding.ASCII.GetString(bytesFromClient);
                    dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf('$'));
                    Console.WriteLine(">> From Client (" + clientNumber + ") Data: " + dataFromClient);

                    serverResponse = "Server to Client (" + clientNumber + ") <ACK>";
                    sendBytes = Encoding.ASCII.GetBytes(serverResponse);
                    networkStream.Write(sendBytes, 0, sendBytes.Length);
                    networkStream.Flush();
                    Console.WriteLine(">> " + serverResponse);
                }
                catch(Exception e)
                {
                    Console.WriteLine(">> " + e.ToString());
                }

            }
        }
    }
}
