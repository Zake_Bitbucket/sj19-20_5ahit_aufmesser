﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(">> Client Started!");
            TcpClient tcpClient = new TcpClient();
            tcpClient.Connect("127.0.0.1", 2345);

            NetworkStream networkStream = tcpClient.GetStream();

            byte[] outStream = Encoding.ASCII.GetBytes("Message from Client$");
            networkStream.Write(outStream, 0, outStream.Length);
            networkStream.Flush();


            byte[] inStream = new byte[65536];
            networkStream.Read(inStream, 0, tcpClient.ReceiveBufferSize);
            string returnData = Encoding.ASCII.GetString(inStream);
            returnData = returnData.Substring(0, returnData.IndexOf("<ACK>"));
            Console.WriteLine("Data from Server: " + returnData);
            Console.ReadKey();
        }
    }
}
