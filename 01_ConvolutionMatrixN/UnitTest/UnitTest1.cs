﻿using System;
using _01_ConvolutionMatrixN;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMatrixCalculation()
        {
            SMatrix input = new SMatrix(8, 10);
            SMatrix kernel = new SMatrix(3, 3);
            int[,,] compare = new int[1, 6, 8] 
            {
                {
                    { 9, 9, 9, 9, 9, 9, 9, 9, },
                    { 9, 9, 9, 9, 9, 9, 9, 9, },
                    { 9, 9, 9, 9, 9, 9, 9, 9, },
                    { 9, 9, 9, 9, 9, 9, 9, 9, },
                    { 9, 9, 9, 9, 9, 9, 9, 9, },
                    { 9, 9, 9, 9, 9, 9, 9, 9, },
                },
            };

            input.FillMatrixWithNumber(1);
            kernel.FillMatrixWithNumber(1);

            SMatrix output = input * kernel;
            //Console.WriteLine(output.ToString());
            bool flag = true;
            for (int z = 0; z < output._depth; z++)
            {
                for (int x = 0; x < output._rows; x++)
                {
                    for (int y = 0; y < output._columns; y++)
                    {
                        Console.Write(compare[z, x, y]);
                    }
                }
            }

            Console.WriteLine();

            for (int z = 0; z < output._depth; z++)
            {
                for (int x = 0; x < output._rows; x++)
                {
                    for (int y = 0; y < output._columns; y++)
                    {
                        Console.Write(output.matrix[x, y, z]);
                    }
                }
            }


            for (int z = 0; z < output._depth; z++)
            {
                for (int x = 0; x < output._rows; x++)
                {
                    for (int y = 0; y < output._columns; y++)
                    {
                        if (output.matrix[x, y, z] != compare[z, x, y])
                            flag = false;
                    }
                }
            }

            Assert.IsTrue(flag);
        }
    }
}
