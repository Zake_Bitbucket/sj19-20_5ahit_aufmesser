﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_ConvolutionMatrixN
{
    public class SMatrix
    {
        public int[,,] matrix;
        public int _rows { get; set; }
        public int _columns { get; set; }
        public int _depth { get; set; } = 1;

        public SMatrix(int rows, int columns, int depth)
        {
            _rows = rows;
            _columns = columns;
            _depth = depth;
            matrix = new int[rows, columns, depth];
        }

        public SMatrix(int rows, int columns)
        {
            _rows = rows;
            _columns = columns;
            matrix = new int[rows, columns, _depth];
        }

        public static SMatrix operator *(SMatrix input, SMatrix kernel)
        {
            int rows = input._rows - (kernel._rows - 1);
            int columns = input._columns - (kernel._columns - 1);
            SMatrix result = new SMatrix(rows, columns, input._depth);

            for (int z = 0; z < input._depth; z++)
            {
                for (int x = 0; x < rows; x++)
                {
                    for (int y = 0; y < columns; y++)
                    {
                        int spe_result = 0;
                        for (int xk = 0; xk < kernel._rows; xk++)
                        {
                            for (int yk = 0; yk < kernel._columns; yk++)
                            {
                                spe_result += input.matrix[x + xk, y + yk, z] * kernel.matrix[xk, yk, 0];
                            }
                        }
                        result.matrix[x, y, z] = spe_result;
                    }
                }
            }
            return result;
        }

        public void FillMatrixWithRandom()
        {
            Random random = new Random();
            for (int z = 0; z < _depth; z++)
            {
                for (int x = 0; x < _rows; x++)
                {
                    for (int y = 0; y < _columns; y++)
                    {
                        matrix[x, y, z] = random.Next(1, 100);
                    }
                }
            }
        }

        public void FillMatrixWithNumber(int number)
        {
            for (int z = 0; z < _depth; z++)
            {
                for (int x = 0; x < _rows; x++)
                {
                    for (int y = 0; y < _columns; y++)
                    {
                        matrix[x, y, z] = number;
                    }
                }
            }
        }

        public override string ToString()
        {
            string output = "";
            for (int z = 0; z < _depth; z++)
            {
                for (int x = 0; x < _rows; x++)
                {
                    for (int y = 0; y < _columns; y++)
                    {
                        output += matrix[x, y, z] + " | " ;
                    }
                    output += "\n";
                }
                output += "\n\n";
            }
            return output;
        }
    }
}
