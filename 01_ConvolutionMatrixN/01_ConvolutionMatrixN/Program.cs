﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_ConvolutionMatrixN
{
    class Program
    {
        static void Main(string[] args)
        {
            SMatrix input = new SMatrix(10000, 10000, 1);
            SMatrix kernel = new SMatrix(100, 100);

            input.FillMatrixWithRandom();
            kernel.FillMatrixWithRandom();

            //input.FillMatrixWithNumber(1);
            //kernel.FillMatrixWithNumber(1);

            //Console.Write(input.ToString());
            //Console.Write(kernel.ToString());
            Console.ReadKey();


            Stopwatch sw = new Stopwatch();

            sw.Start();

            SMatrix output = input * kernel;

            sw.Stop();

            Console.WriteLine("Elapsed={0}\n\n", sw.Elapsed);

            //Console.Write(output.ToString());
            Console.ReadKey();
        }
    }
}
